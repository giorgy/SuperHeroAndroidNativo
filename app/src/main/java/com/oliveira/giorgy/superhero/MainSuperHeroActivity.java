package com.oliveira.giorgy.superhero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import model.SuperHero;
import service.SuperHeroService;

public class MainSuperHeroActivity extends AppCompatActivity {

    EditText edt_consulta_super_hero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_super_hero);
        this.edt_consulta_super_hero = findViewById(R.id.edt_consulta_super_hero);

    }

    public void getSuperHero(View view) {
        String id = edt_consulta_super_hero.getText().toString();

        try {

            SuperHero return_super_hero = new SuperHeroService(id).execute().get();
            Intent intent = new Intent(MainSuperHeroActivity.this, PowerStatsActivity.class);

            intent.putExtra("SuperHeroPowerStats", return_super_hero);
            this.startActivity(intent);


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            Toast.makeText(MainSuperHeroActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
