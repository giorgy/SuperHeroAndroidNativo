package com.oliveira.giorgy.superhero;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import model.SuperHero;

public class PowerStatsActivity extends Activity {

    FloatingActionButton fab_power_stats_share;
    ListView lv_power_stats_hero;
    SuperHero super_hero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_stats);

        this.fab_power_stats_share = findViewById(R.id.fab_power_stats_share);
        this.lv_power_stats_hero = findViewById(R.id.lv_power_stats_hero);

        //obter a intent que iniciou esta activity
        Intent intent = this.getIntent();
        this.super_hero = (SuperHero) intent.getSerializableExtra("SuperHeroPowerStats");

        ArrayList power_stats = new ArrayList<String>();
        power_stats.add("Name: \n" + this.super_hero.getName());
        power_stats.add("Intelligence: \n" + this.super_hero.getIntelligence());
        power_stats.add("Strength: \n" + this.super_hero.getStrength());
        power_stats.add("Combat: \n" + this.super_hero.getCombat());
        power_stats.add("Durability: \n" + this.super_hero.getDurability());
        power_stats.add("Power: \n" + this.super_hero.getPower());


        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, power_stats);
        this.lv_power_stats_hero.setAdapter(adapter);

    }


    public void shareSuperHero(View view) {

        Intent intentCompartilhar = new Intent(Intent.ACTION_SEND);
        intentCompartilhar.setType("text/plain");

        intentCompartilhar.putExtra(Intent.EXTRA_TEXT, this.super_hero.toString());
        intentCompartilhar.putExtra(Intent.EXTRA_SUBJECT, "compart. CEP");

        startActivity(intentCompartilhar);
    }

}
