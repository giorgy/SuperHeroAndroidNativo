package service;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import model.SuperHero;

public class SuperHeroService extends AsyncTask<Void, Void, SuperHero> {

    private String id_super_hero;

    public SuperHeroService(String id_super_hero) {
        Log.d("AST", "construtor SuperHero");
        int id = Integer.parseInt(id_super_hero);
        if (id_super_hero != null && id > 0 && id < 731) {
            this.id_super_hero = id_super_hero;
        } else {
            throw new IllegalArgumentException("Id Hero Invalid");
        }
    }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("AST", "onPreExecute");

        }

        @Override
        protected void onPostExecute(SuperHero super_hero) {
            super.onPostExecute(super_hero);
            Log.i("AST", "onPostExecute");

        }

        @Override
        protected SuperHero doInBackground(Void... voids) {
            Log.i("AST", "doInBackground");
            StringBuilder resposta = new StringBuilder();

            try{
                URL url = new URL("http://www.superheroapi.com/api.php/2315823278442914/"+this.id_super_hero+"/powerstats");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.connect();

                Scanner scanner = new Scanner(url.openStream());

                while (scanner.hasNext()){
                    resposta.append(scanner.next());
                }

            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return new Gson().fromJson(resposta.toString(), SuperHero.class);
    }
}
